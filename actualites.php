<?php include "./partials/connection.php"; $results = connection('news'); ?>

<!DOCTYPE html>
<html>
        <head>
                <meta charset="utf-8"/>
                <title>ARCHIFRANCE</title>
                <link rel="stylesheet" href="./css/style.css">
                <link rel="stylesheet" href="./css/actualites.css">
                <link rel="shortcut icon" href="./img/favicon/favicon.ico" type="image/x-icon">
        </head>

        <body>

        <?php include "./partials/header.php" ?>

        <main>
                <h1>ARCHIFRANCE VOTRE ARCHITECTE A LYON</h1>

                <p class="paragraph">Architecte expérimenté depuis plus de 30 ans, Claude Veyret a réalisé près de 110 projets d’habitats individuels. <br>

                Cet architecte diplômé par le gouvernement (DPLG) travaille pour des particuliers (maison ou appartement) comme pour des professionnels (bureaux, cabinet médical…).<br>
                Vous pouvez faire appel à Claude Veyret pour un projet de construction, d’agrandissement ou d’aménagement.

                L’architecte travaille aussi bien sur des projets de construction de maisons modernes que sur des projets de restructuration ou d’extensions de maisons ou de locaux anciens. <br>

                Il a notamment à son actif création de très nombreux lofts dans d’anciens bâtiments industriels, des rénovations ou extensions de maisons anciennes, l’installation d’habitations dans d’anciens corps de ferme…</p>

                <h1>NOS REALISATIONS</h1>

                <h2>Découvrez les réalisations de notre cabinet d’architectes !</h2>

                <div class="pictures">
                <?php while ($row = $results->fetch()) { ?>

                        <div>
                                <p class="bold"><?php echo $row['texte'];?> :</p> <br>

                                <img class="pictures-news" onclick="agrandirImages(this)" src="<?php echo $row['picture'];?>" alt="<?php echo $row['texte'];?>" width="400" height="380">
                        </div>
                        
                <?php 
                        } 
                        ?>

                        <div id="my-modal" class="my-modal">

                                <span id="close-button" class="close-button">&times;</span>

                                <img id="image-modal" class="image-modal">

                        </div>
                        
                </div>

                <p class="paragraph" >
                        Notre créativité qui s’exprime par la mise en oeuvre de matériaux adaptés à des équilibres de volumes pour répondre à des
                        fonctions et à l’insertion du bâti dans le cadre de vie.</p> <br>

                <p class="paragraph">
                Notre technicité qui permet de réaliser des oeuvres de qualité d’usage et de maintenance dans le respect des coûts et délais,
                en mettant en avant des techniques innovantes, des structures bois ou des énergies renouvelables (solaire, bois).</p> <br>

                <p class="paragraph">
                Notre travail en équipe qui offre la richesse de la variété des connaissances et des compétences. </p> <br>

                <p class="paragraph">
                Notre sensibilité environnementale, trait d’union des autres motivations, qui enracine notre travail dans son époque.</p> <br>

                <p class="paragraph" >
                L’architecture est une passion que nous souhaitons partager.
                
                Passion du cadre de vie, de la qualité des espaces urbains, paysagés et domestiques.
                
                Passion des projets de société, des projets d’avenir, des nouveaux enjeux et défis auxquels nous sommes confrontés.
                </p>


                <h1>PARUTION PRESSE</h1>

                <p class="paragraph">cime cité caue – bois et architecture durable en savoie <br> 
                        ouvrage publié par le CAUE (conseil d’architecture, d’urbanisme et de l’environnement) de la savoie 2012 <br> 
                        p 40,41,42,43,44 la maison des pêcheurs au viviers du lac</p> <br> <br>

                <p class="paragraph">AMC – le moniteur architecture <br>
                        hors-série « intérieurs 2012» <br>
                        p 40,Pateyarchitectes – bureaux – chambéry</p> <br> <br>

                <p class="paragraph">Exposition itinérante «les alpes – lieu de vie» <br>
                        exposition du 14 mai 2010 au 12 septembre 2010 <br>
                        projet présenté : l’oxygène</p> <br> <br>

                <p class="paragraph">Meran <br>
                        ouvrage publié paru été 2010</p> <br> <br>
                         
                <p class="paragraph">Le bois.com <br>
                        n°01 janvier 2009 <br>
                        la maison de la vigne et du vin </p> <br> <br>

                <p class="paragraph">Cime cité caue – les chartes architecturales et paysagères de savoie <br>
                        ouvrage publié par le CAUE (conseil d’architecture, d’urbanisme et de l’environnement) de la savoie 2009 <br>
                        couverture et p 10,19,42,43,50,53,54,55,59,60,61,75,76</p> <br> <br>
        </main>

        <?php include "./partials/footer.php" ?>
        

        </body>
        <script src="./js/agrandissement-image.js"></script>
</html>