<?php include "./partials/connection.php"; $results = connection('products'); ?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" type="text/css" href="./css/style.css">
        <link rel="stylesheet" type="text/css" href="./css/projets.css">
        <link rel="shortcut icon" href="./img/favicon/favicon.ico" type="image/x-icon">
        <title>Archifrance : cabinet d'architecture</title>
    </head>

    <?php include("./partials/header.php"); ?>

    <body>
        <main class="projects">
            <?php while ($row = $results->fetch()) { ?>
                    <section>
                        <div class="section-left">
                            <h2>Type de projet :</h2> 
                            <p><?php echo $row['project']; ?></p>
                            <h2>Lieu :</h2>
                            <p><?php echo $row['location']; ?></p>
                            <h2>Date :</h2>
                            <p><?php echo $row['Date']; ?></p>
                        </div>
                        <div class="section-line">

                        </div>
                        <div class="section-middle">
                            <h2>Coût :</h2>
                            <p><?php echo $row['price']; ?></p>
                            <h2>Missions :</h2>
                            <p><?php echo $row['Assignments']; ?></p>
                            <h2>Client :</h2>
                            <p><?php echo $row['customer']; ?></p>
                        </div>
                        <div class="section-right">
                            <img class="pictures-projects" src="<?php echo $row['picture']; ?>" alt="<?php echo $row['alt_picture']; ?>">
                        </div> 
                    </section>
                    <?php
                }

                // Deconnexion
                $db = null;
            ?>
        </main>
    
        <?php include("./partials/footer.php"); ?>

    </body>
</html>