<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" type="text/css" href="./css/style.css">
        <link rel="stylesheet" type="text/css" href="./css/index.css">
        <link rel="shortcut icon" href="./img/favicon/favicon.ico" type="image/x-icon">
        <title>Archifrance : cabinet d'architecture</title>
    </head>

    <?php include("./partials/header.php"); ?>

    <body>
        <main>
            <div class="slideshow">
                <h1>Bienvenue sur Archifrance</h1>
                <ul class="slideshow-content">
                    <li><img src="img/projets/Residence-privee-slider1.png" alt="Résidence privé à Montesqieu les Albères" width="900px" height="600px"></li>
                    <li><img src="img/projets/Bastide-d-Olette-slide1-projet-inca-architectes.png" alt="Bibliothèque municipale à Tassin-la-demi-lune" width="900px" height="600px"></li>
                    <li><img src="img/projets/Fonseranes1-inca-projet.png" alt="Restaurant à Saint-fons" width="900px" height="600px"></li>
                    <li><img src="img/projets/grant-lemons-80424-unsplash.png" alt="Immeuble de bureaux à Villeurbanne" width="900px" height="600px"></li>
                </ul>
                
            </div>
            
        </main>
    
        <?php include("./partials/footer.php"); ?>

    </body>
</html>