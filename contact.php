<?php 
    include "./partials/connection.php";
    $user_results = connection('user');
    $message_results = connection('message');
?>    
<!DOCTYPE html>
<html lang="fr">
   <head>
       <title>contact</title>
       <link href="./css/style.css" rel="stylesheet" type="text/css">
       <link href="./css/contact.css" rel="stylesheet" type="text/css">
       <link rel="shortcut icon" href="./img/favicon/favicon.ico" type="image/x-icon">
       <meta charset="UTF-8">

   </head>
   <body>
       
        <?php include "./partials/header.php" ?>

        <main class="contact-page">
               
                    
            <h1>Contacter l'équipe :</h1>
            <div class='contact-table'>
                <?php while ($row = $user_results->fetch()): ?> 
                <p> <a href="team.php#<?php echo ($row['lien']);?>"><?php echo ($row['first_name']);?> <?php echo ($row['last_name']);?><br> <?php echo ($row['telephone']);?> </a><br> </p>
                <?php endwhile; ?>
            </div>
            
            <div class="contact-mail-form">
                <h1>Nous contacter :</h1>    
                <form class="mail-form" method="post" action="this">
                  <input type="text" name="last-name" placeholder="Nom de famille">
                  <input type="text" name="first-name" placeholder="Prénom">
                  <input type="tel" name="telephone" placeholder="Téléphone">
                  <input type="email" name="mail" placeholder="Mail">
                  <textarea name="message" placeholder="Rédigez votre commentaire..."></textarea>
                  <input class="submit-button" type="submit" name="submit">
                </form>
                <div class="contact-infos">
                  <h2>ADRESSE :</h2>
                  <p>
                    43 rue de la Liberté,<br>
                    69003 LYON<br>
                    04 78 60 21 07<br>
                    contact@archifrance.fr
                  </p>
                </div>
            </div>  
            <iframe class="map-position" src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d2783.675377784028!2d4.840408750996047!3d45.7576535217329!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1svieux+campeur+lyon!5e0!3m2!1sfr!2sfr!4v1549281620116" width="700" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>                       
        </main>
    
        <?php include "./partials/footer.php" ?>  

   </body>
</html>