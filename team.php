<?php include "./partials/connection.php"; $results = connection('user');?>

<!DOCTYPE html>
<html lang="fr">
	<head>
		<title>ArchiFrance</title>
		<meta charset="utf-8">
		<!--<meta http-equiv="refresh" content="15"> -->
		<link rel="stylesheet" type="text/css" href="./css/style.css">
		<link rel="stylesheet" type="text/css" href="./css/team.css">
		<link rel="shortcut icon" href="./img/favicon/favicon.ico" type="image/x-icon">

		<script type="text/javascript" src="./js/team.js"></script>

	</head>
	
	<body>
		<?php include "./partials/header.php"; ?>
		
		<main class="team-page">
			<?php while($row = $results->fetch()): ?>
			<div class="personnal" id="adrien_dupont">
				<div class="round-image">
					<img id="img-adrien" src="<?php echo $row['image']; ?>" alt="<?php echo $row['description'] ?>">
					<button class="click-profile-button" id="button-principal" onclick=displayButtonProfil("<?php echo $row['image']; ?>")>Voir Profil</button>
				</div>
				
				<h2><?php echo $row['first_name'] ?> <?php echo $row['last_name'] ?></h2>
				<p><?php echo str_replace("µ", "<br>", $row['Texte']); ?></p>
			</div>

			<div class="presentation-background" id="background-emergence">
				<div class="infos-container">
					<img id="img-form" src="">
					<span class="close-icon" id="close-action">&times;</span>
				</div>
			</div>

			<?php endwhile; ?>

		</main>

		<?php include "./partials/footer.php"; ?>	
	
	</body>
</html>
