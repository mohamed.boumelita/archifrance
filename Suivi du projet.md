# Projet : Construction d'un site pour une entreprise fictive #

Faire quatres pages ou plus (actualité, équipe, produit, contacts).
Une page par coéquipier.
Choisir un site et reproduire son design.


# Notre Projet #

## Créer le site d'un cabinet d'architecte ##

Le cabinet d'architecture est basé à Lyon, est spécialisé dans les batiments du tertiaires : maisons, immeubles, bureaux.
Equipe de 9 personnes.
Nom : ArchiFrance.


## Le site est inspiré du site de caterpillar ##

[cat.com](https://www.cat.com/fr_FR.html)

## Choix de l'organisation du code ##

Il a été choisi de faire une branche master et une branche develop

## Répartition des pages web ##

4 Pages : 

1. Actualités : Mohamed
2. Equipe : Quentin
3. Produit : Clément
4. Contact : Valérie

## Organisation des fichiers css ##

1 fichier css pour les parties communes et 1 fichier css par page

## Nommage des classes ##

En anglais.
Les mots séparés par des tirets simples : -

## Nommage des fichiers css ##

"Nom du fichier html".css

## Animations Javascript ##

    * Animation sur la page actualité : quand on clique sur une des cinqs photos, l'image s'affiche en plus grand avec le fond qui s'assombrit. 
    * Animation sur la page équipe : quand on passe sur une photo, un bouton apparait et on pourra cliquer dessus pour voir plus d'informations

## Etapes : ##

1. Créer la base du site : header et footer
    * Header : Clément et Mohamed
    * Footer : Valérie et Quentin
2. Temps de réunion entre nous
2. Réunir le code html du header et du footer en un seul fichier html qui servira de base aux reste des pages
3. Réunir le css du header et du footer dans un seul fichier "style.css"
3. Créer chacun notre "main"/ corps de page ( actu, equipe, produit, contact)
4. Temps de réunion entre nous pour définir les paires pour les animations javascript
    * Animation sur la page actualité : Mohamed et Clément
    * Animation sur la page equipe : Valérie et Quentin
5. Réalisation des animations 
6. Ajout d'une base de donnees sqlite 
7. Modifications des pages : utilisation des infos dans la base de données
8. A suivre

