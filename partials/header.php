<header>
    <div class="header-top">
        <a class="pictures-link" href="index.php">
            <h1>ARCHI</h1>
            <img class="logo-archifrance" src="img/header/img_logo.png" alt="logo archifrance">
            <h1>FRANCE</h1>
        </a>

        <nav>
            <div class="elt-navbar border-left border-right"><a href="actualites.php">ACTUALITES</a></div>
            <div class="elt-navbar border-right"><a href="team.php">EQUIPE</a></div>
            <div class="elt-navbar border-right"><a href="projets.php">PRODUITS</a></div>
            <div class="elt-navbar border-right"><a href="contact.php">CONTACT</a></div>
        </nav>
        <a class="pictures-link" href="#" ><img class="logos-header-right logo-user" src="img/header/logo-utilisateur.png" alt="image utilisateur"></a>
        <a class="pictures-link" href="#" ><img class="logos-header-right logo-earth" src="img/header/depositphotos_106294068-stock-illustration-planet-earth-logo.png" alt="planète"></a>
        <p>Produits et services</p>
        <a class="pictures-link" href="#" ><img class="logos-header-right logo-search" src="img/header/logo-loupe.png" alt="loupe recherche"></a>
    </div>
    <div class="header-bottom">
        <div class="header-bottom-line orange">

        </div>
        <div class="header-bottom-line yellow">

        </div>
    </div>
    

</header>