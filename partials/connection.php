<?php

	function connection($table) {
		try{
			$bdd = new PDO('sqlite:'.dirname(__FILE__).'/../database/database.db');
			$bdd->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
			$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // ERRMODE_WARNING | ERRMODE_EXCEPTION | ERRMODE_SILENT
		} catch(Exception $e) {
			echo "Impossible d'accéder à la base de données SQLite : ".$e->getMessage();
			die();
		}
		$results = $bdd->query('SELECT * FROM '.$table.'');
		return $results;
	}
 

?>