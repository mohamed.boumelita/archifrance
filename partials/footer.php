<footer>
    <div class="archifrance-infos-footer">
        <div class="archi-list">
                <div class="actuality container">
                    <h5 data-toggle-moblile-group="toggle-actuality">Actualités</h5>
                    <div data-toggle-mobile-group="toggle-actuality">
                        <ul>
                            <li><a href="#">Présentation</a></li>
                            <li><a href="#">Nos Réalisations</a></li>
                            <li><a href="#">Parution presse</a></li>
                        </ul>
                    </div>
                </div>
            <div class="team container">
                <h5 data-toggle-mobile-group="toggle-team">Équipe</h5>
                <div data-toggle-mobile-group="toggle-team">
                    <ul>
                        <li><a href="team.php#adrien_dupont">Adrien Dupont</a></li>
                        <li><a href="team.php#cecile_amarande">Cécile Aramande</a></li>
                        <li><a href="team.php#bertrand_plastic">Bertrand PLastic</a></li>
                        <li><a href="team.php#maxime_cooper">Maxime Cooper</a></li>
                        <li><a href="team.php#leonard_fariello">Léonard Fariello</a></li>
                        <li><a href="team.php#laeticia_benedetti">Laeticia Benedetti</a></li>
                        <li><a href="team.php#coralie_pons">Coralie Pons</a></li>
                        <li><a href="team.php#robert_deusvult">Robert Deus-Vult</a></li>
                        <li><a href="team.php#rajesh_koothrappali">Rajesh Koothrappali</a></li>
                    </ul>
                </div>
            </div>	
            <div class="products container">
                <h5 data-toggle-mobile-group="toggle-products">Produits</h5>
                <div data-toggle-mobile-group="toggle-products">
                    <ul>
                        <li><a href="projets.php#residence">Résidence</a></li>
                        <li><a href="projets.php#restaurant">Restaurant</a></li>
                        <li><a href="projets.php#library">Bibliothèque</a></li>
                        <li><a href="projets.php#building">Immeuble</a></li>
                    </ul>
                </div>
            </div>
            <div class="contacts container">
                <h5 data-toggle-mobile-group="toggle-contacts">Contacts</h5>
                <div data-toggle-mobile-group="toggle-contacts">
                    <ul>
                        <li><a class="social-icon fb" href="https://fr-fr.facebook.com/" target="_blank">Facebook</a></li>
                        <li><a class="social-icon gp" href="https://plus.google.com/discover" target="_blank">Google Plus</a></li>
                        <li><a class="social-icon ln" href="https://fr.linkedin.com/" target="_blank">Linkedln</a></li>
                        <li><a class="social-icon tw" href="https://twitter.com/?lang=fr" target="_blank">Twitter</a></li>
                        <li><a class="social-icon yt" href="https://www.youtube.com/" target="_blank">Youtube</a></li>
                        <li>
                            <img src="https://badges.instagram.com/static/thirdparty/images/badges/ig-badge-16.png/1f6a7ba1a929.png">
                            <a href="https://www.instagram.com/?hl=fr" target="_blank">Instagram</a>
                        </li>
                        <li><a class="contacts-page" href="../contact.php" target="_blank">Autres Contacts</a></li>
                    </ul>
                </div>
            </div>
        </div>	
    </div>

</footer>